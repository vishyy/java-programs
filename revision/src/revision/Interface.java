package revision;

import java.lang.reflect.Proxy;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class Interface {
	public static void main(String[] args) {
		AyurvedaMedicalCollege ayush=new AyurvedaMedicalCollege();
		JetAcademy jet =new JetAcademy();
		Object c=Proxy.newProxyInstance(Human.class.getClassLoader(), 
				new Class[] {Doctor.class,Pilot.class},new MyInvocationHandler(new Object[] {ayush,jet}));
		Doctor d=(Doctor)c;
		d.doCure();
		Pilot p=(Pilot)c;
		p.fly();
		
	}
}
class MyInvocationHandler implements InvocationHandler{
	Object object[];
	Object o;
	 public MyInvocationHandler(Object object[]) {
		this.object=object;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// TODO Auto-generated method stub
		for(int i=0;i<object.length;i++)
		{
			try {
				o=method.invoke(object[i], args);
			}
			catch(Exception e) {
				//System.out.println(e);
			}
		}
		return o;
	}
	
}
class Human{}

interface Doctor{
	void doCure();
	void doGiveMedicine();
}

class AyurvedaMedicalCollege implements Doctor{
	@Override
	public void doCure() {
		System.out.println("ayurved cure for corona started...");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("kabasura neer given to cure.......");
	}
}
interface Pilot{
	void fly();
}

class JetAcademy implements Pilot{
	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("he is flying");
	}
}