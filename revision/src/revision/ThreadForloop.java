package revision;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadForloop {
	public static void main(String[] args) {
		ExecutorService es=Executors.newFixedThreadPool(3);
		Army a=new Army();
		es.execute(()->{
			for(int i=0;i<5;i++)
				a.pull();
		});
		
		es.execute(()->{
			for(int i=0;i<5;i++)
				a.fire();
		});
	}
}

class Army{
	boolean flag=false;
	synchronized public void pull() {
		if(flag) {
			
			try {
				wait();
			}catch(Exception e) {}
		}
		System.out.println("pull called");
			flag=true;
			notify();
	}

	
	synchronized public void fire() {
		if(!flag)
		{
			try {
				wait();
				
			}catch(Exception e) {}
		}
		System.out.println("fire called");
			flag=false;
			notify();
	
	}
}