package revision;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethod {
	public static void main(String[] args) throws Exception{
		Politician p1=new Politician();
		Police police=new Police();
		police.arrest(p1);
	}
}
class Police{
	public void arrest(Object p) throws Exception {
		Class c=p.getClass();
		Field field=c.getField("name");
		System.out.println(field.get(p));
		field=c.getDeclaredField("secretname");
		field.setAccessible(true);
		System.out.println(field.get(p));
		
		Method meth=c.getMethod("work");
		meth.invoke(p);
		
	}
}

class Politician{
	public String name="iam holy man";
	private String secretname="rowdy";
	public void work() {
		System.out.println("i do social work");
	}
	public void secretWork() {
		System.out.println("murder");
	}
}